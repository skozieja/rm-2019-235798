import numpy as np
import math
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random

#definiowanie przestrzeni robota
delta = 0.01
x_size = 10
y_size = 10

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

#definiowanie potencjalow
vektor_przeszkody = []
for i in range(4):
    vektor_przeszkody.append( (random.randrange(-10,10), random.randrange(-10, 10)))

start_point=(-10,random.randrange(-10,10))
finish_point=(10,random.randrange(-10,10))
while finish_point in vektor_przeszkody:
    finish_point = (10, random.randrange(-10, 10))

#definicja wspolczynnikow dla przeszkod
koi = 10
kp = 100000

#obliczenie potencjalu w kazdym punkcie
for i in range(Z.shape[0]):
    for j in range(Z.shape[1]):
        sum = 0.
        for k in vektor_przeszkody:
            r = ( np.array(k) - np.array([x[j],y[i]]))
            norma = math.hypot(r[0],r[1])
            if norma < 2:
                sum += 1/2*(1/norma**2 - 1/2)
        if sum > 100: #ograniczenie maksymalnego potencjalu
            sum = 100
        r = np.array(finish_point - np.array([x[j],y[i]]))
        norma = math.hypot(r[0],r[1])
        sum += koi*1/2*norma**2
        Z[i][j] = sum
    print(i)

trace = []
trace.append(start_point)
#obliczenie sciezki
if start_point[1] >= 0:
    position = (0,start_point[0]*100-1+1000)
else:
    position = (0,-start_point[0]*100-1)
last_found = position
if finish_point[1] >= 0:
    finish = (finish_point[1]*100-1+1000,1999)
else:
    finish = (-finish_point[1]*100-1, 1999)
l = 0
while position is not finish and l < 4000:
    l += 1
    print(f"finish point: {finish_point}, finish {finish}")
    for i in range(-1,2):
        for j in range(-1,2):
            print(f"is {position[0]+i} and {position[1]+j}")
          #  print(f"p_{position[0]+j}p_{position[1]}z_{Z.shape[0]}z_{Z.shape[1]}")
            if position[0]+i < Z.shape[0] and position[1]+j < Z.shape[1] and position[0]+i >=0 and position[1]+j>=0:
                if Z[last_found[0]][last_found[1]] > Z[position[0]+i][position[1]+j]:
                    last_found = (position[0]+j,position[1]+i)
                print(f"last{Z[last_found[0]][last_found[1]]} and pos{Z[position[0]+i][position[1]+j]}")
    trace.append((x[last_found[1]], y[last_found[0]]))
    position = last_found
    print(last_found)
print(start_point)
print(finish_point)

print("calculated")
#plotowanie wyniku dzialania skryptu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='uper', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')
print(trace)

for points_of_way in trace:
    plt.plot(points_of_way[0],points_of_way[1], "or", color='red')

for przeszkoda in vektor_przeszkody:
    plt.plot(przeszkoda[0], przeszkoda[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
